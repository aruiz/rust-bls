// bls.rs
//
// Copyright 2022 Alberto Ruiz <aruiz@gnome.org>
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.
//
// SPDX-License-Identifier: MPL-2.0

//! Implements systemd's [Boot Loader Specification](https://systemd.io/BOOT_LOADER_SPECIFICATION/)
//!
//! This API can parse and modify a Boot Loader Spec entry file.
//! It supports the [Fedora/GRUB specific commands](https://fedoraproject.org/wiki/Changes/BootLoaderSpecByDefault).
//!
//! This library can be used in a no_std environment that
//! supports dynamic memory allocation by disabling
//! the "std" feature using --no-default-features.
//!
//! NOTE: At the moment, if you parse a BLSEntry with full-line
//! comments and write it back, all comment lines will be consolidated
//! in the header.

#![cfg_attr(not(feature = "std"), no_std)]
#![cfg(not(feature = "std"))]
extern crate alloc;

#[cfg(not(feature = "std"))]
use alloc::format;
#[cfg(not(feature = "std"))]
use alloc::string::String;
#[cfg(not(feature = "std"))]
use alloc::vec::Vec;

#[cfg(not(feature = "std"))]
use alloc::str::FromStr;
#[cfg(feature = "std")]
use std::str::FromStr;

/// BLSValue generalizes a value that may have an inline comment
#[derive(Debug, PartialEq)]
pub enum BLSValue {
    /// The string represent the argument string for the BLS command
    Value(String),
    /// Holds string that represents the BLS command argument as well as a comment string at the end of the line
    ValueWithComment(String, String),
}

/// This enum is used as an argument in the ```BLSEntry::set()``` method.
/// Some keys like ```initrd``` can be specified multiple times in a BLS entry file.
#[derive(Debug, PartialEq)]
pub enum ValueSetPolicy {
    /// This replaces all existing values for the given key
    ReplaceAll,
    /// This appends a value to the last position in the BLSEntry
    Append,
    /// This inserts a value in the first position in the BLSEntry
    Prepend,
    /// This inserts a value in the given index. Note that his may cause a panic as per ```std::vec::Vec::insert()```
    InsertAt(usize),
}

/// List of available BLS keys
#[derive(Debug, PartialEq)]
pub enum BLSKey {
    Title,
    Version,
    MachineId,
    SortKey,
    Linux,
    Efi,
    Initrd,
    Options,
    Devicetree,
    DevicetreeOverlay,
    Architecture,
    GrubHotkey,
    GrubUsers,
    GrubClass,
    GrubArg,
}

impl FromStr for BLSKey {
    type Err = String;

    fn from_str(key: &str) -> Result<Self, Self::Err> {
        match key {
            "linux" => Ok(BLSKey::Linux),
            "title" => Ok(BLSKey::Title),
            "version" => Ok(BLSKey::Version),
            "machine_id" => Ok(BLSKey::MachineId),
            "sort_key" => Ok(BLSKey::SortKey),
            "efi" => Ok(BLSKey::Efi),
            "initrd" => Ok(BLSKey::Initrd),
            "options" => Ok(BLSKey::Options),
            "devicetree" => Ok(BLSKey::Devicetree),
            "devicetree_overlay" => Ok(BLSKey::DevicetreeOverlay),
            "architecture" => Ok(BLSKey::Architecture),
            "grub_hotkey" => Ok(BLSKey::GrubHotkey),
            "grub_users" => Ok(BLSKey::GrubUsers),
            "grub_class" => Ok(BLSKey::GrubClass),
            "grub_arg" => Ok(BLSKey::GrubArg),
            _ => Err(format!("Invalid key {}", key)),
        }
    }
}

/// BLSEntry represents the contents of a BLS entry file
#[derive(Debug)]
pub struct BLSEntry {
    pub title: Option<BLSValue>,
    pub version: Option<BLSValue>,
    pub machine_id: Option<BLSValue>,
    pub sort_key: Option<BLSValue>,
    pub linux: BLSValue,
    pub efi: Option<BLSValue>,
    pub initrd: Vec<BLSValue>,
    pub options: Vec<BLSValue>,
    pub devicetree: Option<BLSValue>,
    pub devicetree_overlay: Option<BLSValue>,
    pub architecture: Option<BLSValue>,
    pub grub_hotkey: Option<BLSValue>,
    pub grub_users: Option<BLSValue>,
    pub grub_class: Vec<BLSValue>,
    pub grub_arg: Option<BLSValue>,
    // NOTE: All comments are moved to the header of the file upon rendering back the content
    pub comments: Vec<String>,
}

impl BLSEntry {
    /// Allocates a new instance of BLSEntry, all optional members are initialized to None and ```linux``` is set with an empty string
    pub fn new() -> BLSEntry {
        BLSEntry {
            title: None,
            version: None,
            machine_id: None,
            sort_key: None,
            linux: BLSValue::Value(String::new()),
            efi: None,
            initrd: Vec::new(),
            options: Vec::new(),
            devicetree: None,
            devicetree_overlay: None,
            architecture: None,
            grub_hotkey: None,
            grub_users: None,
            grub_class: Vec::new(),
            grub_arg: None,
            comments: Vec::new(),
        }
    }

    /// Parses a Boot Loader Spec entry UTF-8 buffer, returns a BLSEntry instance if successful, an error String if there was an error
    /// Note that any comment lines that are then rendered using BLSEntry::parse() will be pushed to the header of the file as the
    /// order commands and comments are not preserved.
    pub fn parse(buffer: &str) -> Result<BLSEntry, String> {
        let mut entry = BLSEntry::new();
        let mut has_linux = false;

        for line in buffer.lines() {
            let mut comment = None;
            // Extract the comment string from the line
            let line = if line.contains("#") {
                let split: Vec<_> = line.splitn(2, "#").collect();
                comment = Some(String::from(split[1]));
                split[0]
            } else {
                line
            };

            // NOTE: For now we put all comment lines in the header
            if line.trim().contains(" ") {
                let key_value: Vec<&str> = line.trim().splitn(2, " ").collect();

                let key = BLSKey::from_str(key_value[0])?;
                if key == BLSKey::Linux {
                    has_linux = true;
                }
                entry.set(
                    key,
                    String::from(key_value[1]),
                    comment,
                    ValueSetPolicy::Append,
                );
            } else {
                match comment {
                    Some(comment) => {
                        entry.comments.push(comment);
                    }
                    None => {}
                }
            }
        }

        if has_linux {
            Ok(entry)
        } else {
            Err(String::from("No 'linux' command found."))
        }
    }

    /// Renders the BLSEntry content into a UTF-8 String
    pub fn render(&self) -> String {
        let mut content = String::new();

        fn render_value(content: &mut String, key: &str, value: &BLSValue) {
            content.push_str(key);
            content.push(' ');
            match value {
                BLSValue::Value(value) => content.push_str(&value),
                BLSValue::ValueWithComment(value, comment) => {
                    content.push_str(&value);
                    content.push_str(" #");
                    content.push_str(&comment);
                }
            }
        }

        fn render_single_value(content: &mut String, key: &str, value: &Option<BLSValue>) {
            if let Some(value) = value {
                render_value(content, key, &value)
            }
        }

        fn render_multiple_values(content: &mut String, key: &str, values: &Vec<BLSValue>) {
            for val in values {
                render_value(content, key, &val)
            }
        }

        // We push all comments in the header
        for comment in &self.comments {
            content.push_str("#");
            content.push_str(&comment)
        }

        // Mandatory commands
        render_value(&mut content, "linux", &self.linux);

        // Optional commands
        render_single_value(&mut content, "title", &self.title);
        render_single_value(&mut content, "version", &self.version);
        render_single_value(&mut content, "machine-id", &self.machine_id);
        render_single_value(&mut content, "sort-key", &self.sort_key);
        render_single_value(&mut content, "efi", &self.efi);
        render_single_value(&mut content, "devicetree", &self.devicetree);
        render_single_value(&mut content, "devicetree-overlay", &self.devicetree_overlay);
        render_single_value(&mut content, "architecture", &self.architecture);
        render_single_value(&mut content, "grub_hotkey", &self.devicetree_overlay);
        render_single_value(&mut content, "grub_users", &self.devicetree_overlay);
        render_single_value(&mut content, "grub_arg", &self.devicetree_overlay);

        // Commands with multiple values
        render_multiple_values(&mut content, "initrd", &self.initrd);
        render_multiple_values(&mut content, "options", &self.options);
        render_multiple_values(&mut content, "grub_class", &self.grub_class);

        content
    }

    /// Sets a value for a given key
    /// # Arguments
    ///
    /// - ```key```: a &str representing the key to be set
    /// - ```value```: a String representing the value for the key
    /// - ```comment```: an optional String reprensenting an inline comment for the value
    /// - ```set_policy```: Some keys can hold multiple values, the ```ValueSetPolicy``` enum specifies the policy for keys that can be specified multiple times.
    ///
    /// # Panics
    ///
    /// If ```ValueSetPolicy::InsertAt(usize)``` is used as ```set_policy``` it may cause a panic if the index is out of bound
    pub fn set(
        &mut self,
        key: BLSKey,
        value: String,
        comment: Option<String>,
        set_policy: ValueSetPolicy,
    ) {
        fn value_generator(value: String, comment: Option<String>) -> BLSValue {
            match comment {
                Some(comment) => BLSValue::ValueWithComment(value, comment),
                None => BLSValue::Value(value),
            }
        }

        fn push_value(values: &mut Vec<BLSValue>, val: BLSValue, policy: ValueSetPolicy) {
            match policy {
                ValueSetPolicy::Append => values.push(val),
                ValueSetPolicy::InsertAt(i) => values.insert(i, val),
                ValueSetPolicy::Prepend => values.insert(0, val),
                ValueSetPolicy::ReplaceAll => {
                    values.clear();
                    values.push(val);
                }
            }
        }

        match key {
            BLSKey::Title => self.title = Some(value_generator(value, comment)),
            BLSKey::Version => self.version = Some(value_generator(value, comment)),
            BLSKey::MachineId => self.machine_id = Some(value_generator(value, comment)),
            BLSKey::SortKey => self.sort_key = Some(value_generator(value, comment)),
            BLSKey::Linux => self.linux = value_generator(value, comment),
            BLSKey::Efi => self.efi = Some(value_generator(value, comment)),
            BLSKey::Devicetree => self.devicetree = Some(value_generator(value, comment)),
            BLSKey::DevicetreeOverlay => {
                self.devicetree_overlay = Some(value_generator(value, comment))
            }
            BLSKey::Architecture => self.architecture = Some(value_generator(value, comment)),
            BLSKey::GrubHotkey => self.grub_hotkey = Some(value_generator(value, comment)),
            BLSKey::GrubUsers => self.grub_users = Some(value_generator(value, comment)),
            BLSKey::GrubArg => self.grub_arg = Some(value_generator(value, comment)),

            BLSKey::Initrd => push_value(
                &mut self.initrd,
                value_generator(value, comment),
                set_policy,
            ),
            BLSKey::Options => push_value(
                &mut self.options,
                value_generator(value, comment),
                set_policy,
            ),
            BLSKey::GrubClass => push_value(
                &mut self.grub_class,
                value_generator(value, comment),
                set_policy,
            ),
        }
    }

    /// Clears a field in the BLSEntry
    /// 
    /// # Arguments
    /// - key: The ```BLSKey``` to clear. Note that if ```BLSKey::linux``` is used it will set it to an empty string as this key is always supposed to be present. Sets None or empty vector otherwise.
    pub fn clear(&mut self, key: BLSKey) {
        match key {
            BLSKey::Linux => self.linux = BLSValue::Value(String::from("")),
            BLSKey::Title => self.title = None,
            BLSKey::Version => self.version = None,
            BLSKey::MachineId => self.machine_id = None,
            BLSKey::SortKey => self.sort_key = None,
            BLSKey::Efi => self.efi = None,
            BLSKey::Devicetree => self.devicetree = None,
            BLSKey::DevicetreeOverlay => self.devicetree_overlay = None,
            BLSKey::Architecture => self.architecture = None,
            BLSKey::GrubHotkey => self.grub_hotkey = None,
            BLSKey::GrubUsers => self.grub_users = None,
            BLSKey::GrubArg => self.grub_arg = None,

            BLSKey::Initrd => self.initrd.clear(),
            BLSKey::Options => self.options.clear(),
            BLSKey::GrubClass => self.grub_class.clear(),
        }
    }
}

#[cfg(test)]
mod bls_tests {
    use super::String;

    #[cfg(not(feature = "std"))]
    use alloc::vec;

    use super::BLSEntry;
    use super::BLSKey;
    use super::BLSValue;
    use super::ValueSetPolicy;

    #[test]
    fn new_entry() {
        let entry = BLSEntry::new();
        match entry.linux {
            BLSValue::Value(linux) => {
                assert_eq!(linux, "");
            }
            _ => {
                panic!("Invalid 'linux' value {:?}", entry.linux);
            }
        }
        assert_eq!(entry.initrd.len(), 0);
    }

    #[test]
    fn parse_entry() {
        let entry_txt = "#Comment\n\
                     linux foobar-2.4\n\
                     options foo=bar #Another Comment";
        let entry = BLSEntry::parse(entry_txt);

        assert!(entry.is_ok());
        let entry = entry.unwrap();
        assert_eq!(entry.comments.len(), 1);
        assert_eq!(entry.comments[0], "Comment");

        if let BLSValue::Value(linux) = entry.linux {
            assert_eq!(linux, "foobar-2.4");
        }

        assert_eq!(entry.options.len(), 1);
        match &entry.options[0] {
            BLSValue::ValueWithComment(option, comment) => {
                assert_eq!(option, "foo=bar");
                assert_eq!(comment, "Another Comment");
            }
            _ => {
                panic!("Invalid 'options' value {:?}", entry.options[0])
            }
        }
    }

    #[test]
    fn parse_errors() {
        // Missing 'linux' command
        let entry_txt = "options foo=bar";
        let entry = BLSEntry::parse(entry_txt);
        assert!(entry.is_err());

        // Invalid command
        let entry_txt = "linux asdasdasdas\n\
                     invalid_command foo=bar";
        let entry = BLSEntry::parse(entry_txt);
        assert!(entry.is_err());
    }

    #[test]
    fn set_value_policies() {
        // Append
        let mut entry = BLSEntry::new();
        let _ = entry.set(
            BLSKey::Options,
            String::from("foo"),
            None,
            ValueSetPolicy::Append,
        );
        let _ = entry.set(
            BLSKey::Options,
            String::from("bar"),
            None,
            ValueSetPolicy::Append,
        );
        let _ = entry.set(
            BLSKey::Options,
            String::from("baz"),
            None,
            ValueSetPolicy::Append,
        );

        assert_eq!(
            entry.options,
            vec![
                BLSValue::Value(String::from("foo")),
                BLSValue::Value(String::from("bar")),
                BLSValue::Value(String::from("baz"))
            ]
        );

        // InsertAt
        let _ = entry.set(
            BLSKey::Options,
            String::from("lol"),
            None,
            ValueSetPolicy::InsertAt(1),
        );
        assert_eq!(
            entry.options,
            vec![
                BLSValue::Value(String::from("foo")),
                BLSValue::Value(String::from("lol")),
                BLSValue::Value(String::from("bar")),
                BLSValue::Value(String::from("baz"))
            ]
        );

        // ReplaceAll
        let _ = entry.set(
            BLSKey::Options,
            String::from("wtf"),
            None,
            ValueSetPolicy::ReplaceAll,
        );
        assert_eq!(entry.options, vec![BLSValue::Value(String::from("wtf"))]);

        // Prepend
        let _ = entry.set(
            BLSKey::Options,
            String::from("uwu"),
            None,
            ValueSetPolicy::Prepend,
        );
        assert_eq!(
            entry.options,
            vec![
                BLSValue::Value(String::from("uwu")),
                BLSValue::Value(String::from("wtf"))
            ]
        );

        // Clear
        entry.clear(BLSKey::Options);
        assert_eq!(entry.options, vec![]);

        entry.set(
            BLSKey::Title,
            String::from("foobar"),
            None,
            ValueSetPolicy::Append,
        );
        assert_eq!(entry.title, Some(BLSValue::Value(String::from("foobar"))));

        entry.clear(BLSKey::Title);
        assert_eq!(entry.title, None);
    }
}
